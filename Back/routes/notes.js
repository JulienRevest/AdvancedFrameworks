const express = require('express');
const router = express.Router();

const {
    getNotes,
    getNote,
    createNote,
    updateNote,
    deleteNote
} = require('../controllers/notes.js');

/**
 * Define our routes and the associated functions
 */

router.get('/', getNotes);

router.get('/single', getNote);

router.post('/', createNote);

router.put('/', updateNote);

router.delete('/', deleteNote);

module.exports = router;