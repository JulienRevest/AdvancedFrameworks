const sqlite3 = require('sqlite3').verbose();

const notes = [
    { id: 1, name: 'iPhone', price: 800 },
    { id: 2, name: 'iPad', price: 650 },
    { id: 3, name: 'iWatch', price: 750 },
]

let db = new sqlite3.Database('noteDatabase', (err) => {
    if(err) {
        return console.error('Error: ' + err.message);
    }
    console.log('Connected to the in-memory SQLite database');
    db.run('CREATE TABLE IF NOT EXISTS "Notes" ('
    + 'id INTEGER PRIMARY KEY,'
    + 'name CHAR(128),'
    + 'text MEDIUMTEXT,'
    + 'status BIT NOT NULL);');

    // db.run('INSERT INTO "Notes" (name, text, status) VALUES ("test", "Small test to add a note", 1);')
    // db.run('INSERT INTO "Notes" (name, text, status) VALUES ("test2", "Small test to add a note", 2);')
    // db.run('INSERT INTO "Notes" (name, text, status) VALUES ("test3", "Small test to add a note", 3);')

    // db.each('SELECT * FROM "Notes";', [], (err, row) => {
    //     if(err) {
    //         return console.error(err.message);
    //     }
    //     return row ?
    //     console.log(row.id, row.name, row.text, row.status)
    //     : console.log('Not found');
    // });
    db.run('SELECT * FROM "Notes";');
});


/**
 * Define our Setters/Getters for the database
 */

let globalNotes = {};
let globalNote;

function setNotes(notes) {
    globalNotes = notes;
}

function getNotes() {
    return new Promise((resolve, reject) => {
        db.each('SELECT * FROM "Notes";', [], (err, row) => {
            if(err) {
                reject(err);
            }
            if(row) {
                globalNotes[row.id] = {name: row.name, text: row.text, status: row.status};
            }
        }, (err, row) => {
            if(err) {
                reject(err);
            }
            resolve(globalNotes);
        });
    });
}

function getSingularNote(noteID) {
    if(!noteID)
    {
        console.log("Incorrect ID!");
        return {error: "Invalid ID"};
    }
    globalNote = null;
    return new Promise((resolve, reject) => {
        db.each('SELECT * FROM "Notes" WHERE id = ?', [noteID], (err, row) => {
            if(err) {
                reject(err.message);
            }
            globalNote = {name: row.name, text: row.text, status: row.status};
        }, (err, row) => {
            if(err) {
                reject(err);
            }
            resolve(globalNote);
        });
    });
    // return notes.find(note => note.id === noteID);
}

function updateNote(noteID, note) {
    if(!noteID)
    {
        console.log("Incorrect ID!");
        return;
    }
    return new Promise((resolve, reject) => {
        db.run('UPDATE "Notes" SET name = ?, text = ?, status = ? WHERE id = ?', [note.name, note.text, note.status, noteID], (err, row) => {
            if(err) {
                reject(err.message);
            }
        }, (err, row) => {
            if(err) {
                reject(err);
            }
        });
    });
}

function addNote(newNote) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO "Notes" VALUES (null, ?, ?, ?)', [newNote.name, newNote.text, newNote.status], (err, row) => {
            if(err) {
                reject(err.message);
            }
        }, (err, row) => {
            if(err) {
                reject(err);
            }
        });
    });
}

function deleteNote(noteToDelete) {
    return new Promise((resolve, reject) => {
        db.run('DELETE FROM "Notes" WHERE id = ?', [noteToDelete], (err, row) => {
            if(err) {
                reject(err.message);
            }
        }, (err, row) => {
            if(err) {
                reject(err);
            }
        });
    });
}

// Export our function for access in other files
module.exports = { getNotes, getSingularNote, updateNote, addNote, deleteNote };