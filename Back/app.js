const express = require('express');
const app = express();
const notes_route = require('./routes/notes');

// Post and Put requests will use JSON
app.use(express.json())

// Start an app and listen on port 5000
app.listen(5000, () => {
    console.log('Server listening on port 5000');
});

/**
 * Defining routes
 * The prefix defined here will be used
 * for all the routes in notes_route
 */

app.use('/api/notes', notes_route);