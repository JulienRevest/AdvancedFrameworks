const db = require('../database/dbManager');


// Get all notes
const getNotes = (async (req, res) => {
    let results = await db.getNotes();
    console.log("GETALL Request from", req.hostname);
    res.json(results);
});

// Get a note with its ID
const getNote = (async (req, res) => {
    console.log("GETSINGLE Request from", req.hostname, "asking for", req.query.id);
    let results = await db.getSingularNote(req.query.id) 
    if(!results) {
        console.log("GETSINGLE Request from", req.hostname, "failed, note doesn't exist");
        res.status(404).json('An error occurred: Note doesn\'t exist');
        return;
    }
    res.json(results);
});

// Update a note with its ID
const updateNote = (async (req, res) => {
    let id = req.query.id;
    let exist = await db.getSingularNote(req.query.id)
    console.log("UPDATE Request from", req.hostname, "updating", req.query.id);
    if(!exist) {
        console.log("UPDATE Request from", req.hostname, "failed, note doesn't exist");
        res.status(404).json('An error occurred: Note doesn\'t exist');
        return;
    }
    let updatedNote = {
        id: id,
        name: req.body.name,
        text: req.body.text,
        status: req.body.status
    };
    db.updateNote(id, updatedNote);
    res.status(200).json('Note updated');
});

// Create a new note
const createNote = (async (req, res) => {
    console.log("CREATE Request from", req.hostname);
    const newNote = {
        name: req.body.name,
        text: req.body.text,
        status: req.body.status
    };
    db.addNote(newNote);
    res.status(200).json('Note added');
});

// Delete a note
const deleteNote = (async (req, res) => {
    console.log("DELETE Request from", req.hostname, "removing", req.query.id);
    db.deleteNote(req.query.id);
    res.status(200).json('Note deleted');
});

module.exports = {
    getNotes,
    getNote,
    createNote,
    updateNote,
    deleteNote
}