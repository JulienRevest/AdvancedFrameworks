import { INotes } from './../../structures/notes';
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NoteService} from "../../services/note.service";
import {Note} from "../../structures/note";

@Component({
  selector: 'app-waiting',
  templateUrl: './waiting.component.html',
  styleUrls: ['./waiting.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WaitingComponent implements OnInit {

  public notes = [];
  constructor(public noteService: NoteService) {
  }

  ngOnInit(): void {
    this.noteService.getNotes()
      .subscribe(data => {
        for(let noteId of Object.keys(data)) {
          if(data[noteId].status == 1) {
            this.notes.push(data[noteId]);
          }
        }
      });
  }

  testGetNotes() {
    for(let noteId of Object.keys(this.notes)) {
      console.log(noteId, this.notes[noteId]);
    }
  }

  setStatus(note: Note, status: string){
    this.noteService.setStatus(note, status);
  }

  detail(note: Note){
    console.log("click");
  }
}
