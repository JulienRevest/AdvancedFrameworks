import { INotes } from './../structures/notes';
import {Injectable} from '@angular/core';
import {Note} from '../structures/note';
import {not} from "rxjs/internal-compatibility";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  public _notes: Map<String, Note>;
  public notes;
  private noteApi: string = '/api/notes'; // The proxy will redirect "/api/notes/*" to "http://localhost:5000/api/notes/"

  // ngOnInit() {
  //   this.http.get(this.noteApi)
  //     .subscribe(notesResponse => {
  //       console.log(notesResponse);

  //       this.notes = notesResponse;
  //     })
  // }

  constructor(private http: HttpClient) {
    this._notes = new Map<String, Note>();
    // this.notes = this.http.get(this.noteApi);
    // getLocaStorageNotes
    for(let i = 0; i < localStorage.length; i++){
      const key = localStorage.key(i);

      // const note = JSON.parse(localStorage.getItem(key)) as Note;
      const note = JSON.parse(localStorage.getItem(key)) as Note;
      this.updateFromStorage(note);
    }
  }

  getNotes(): Observable<INotes[]>{
    return this.http.get<INotes[]>(this.noteApi);
  }

  public updateFromStorage(note: Note){
    this.http.get(this.noteApi);
    const newNote = new Note();
    newNote.setNote(note);
    const existNote = this._notes.get(newNote.titre);
    if(!existNote){
      this._notes.set(newNote.titre, newNote);
    }
  }

  public addNote(note: Note):void {
    const existNote = this._notes.get(note.titre);
    if(existNote){
      existNote.createDate = note.createDate; // date de modification
      existNote.status = "waiting";
      existNote.body = note.body;
      existNote.document = note.document;
      this._notes.set(existNote.titre, existNote);
      localStorage.setItem(existNote.titre.toString(), JSON.stringify(existNote))

    } else{
      if(!note.createDate){
        note.createDate = new Date();
      }
      note.status = "waiting";
      this._notes.set(note.titre, note);
      localStorage.setItem(note.titre.toString(), JSON.stringify(note))

    }
  }

  public resetNote(note: Note): Note{
    const existNote = this._notes.get(note.titre);
    if(existNote) {
      this._notes.delete(note.titre);
      localStorage.removeItem(note.titre.toString());
    }
    note.titre = "";
    note.status = ""
    note.body = ""
    note.document = ""

    return note;
  }

  public getNote(titre: String):Note{
    return  this._notes.get(titre);
  }

  public setStatus(note: Note, status: string){
    const existNote = this._notes.get(note.titre);
    if(existNote) {
      existNote.status= status;
      this._notes.set(existNote.titre, existNote);
      localStorage.setItem(existNote.titre.toString(), JSON.stringify(existNote))
    }
  }

}
