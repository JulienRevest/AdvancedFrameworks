export interface INotes {
  id: number,
  name: string,
  text: string,
  status: number,
}
