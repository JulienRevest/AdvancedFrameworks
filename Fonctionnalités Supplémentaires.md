- Accèder au système de fichiers.
- Gérer le téléchargements de fichiers (ie: avec Multer).
= Créer votre serveur HTTS en obtenant et en gérant un certificat TLS.
- Apprenez à gérer l'authentification JWT et avec des sessions en utilisant des cookies.
- Ajouter un tri (recherche) sur les notes